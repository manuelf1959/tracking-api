package co.com.soaint.tracking.api.repository.trackState;

import co.com.soaint.tracking.api.entities.TrackState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackStateRepository extends JpaRepository<TrackState, String> {

    @Query("select n from TrackState n where n.trackStateId = :id")
    TrackState findById(@Param("id") final Long id);

}
