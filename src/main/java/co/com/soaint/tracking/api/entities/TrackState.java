package co.com.soaint.tracking.api.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sdc_track_state")
public class TrackState {
    private Long trackStateId;
    private String activity;
    private String state;
    private String role;
    private String process;
    private List<Track> tracksByTrackStateId;

    @Id
    @Column(name = "track_state_id")
    public Long getTrackStateId() {
        return trackStateId;
    }

    public void setTrackStateId(Long trackStateId) {
        this.trackStateId = trackStateId;
    }

    @Basic
    @Column(name = "activity")
    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Basic
    @Column(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Basic
    @Column(name = "process")
    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrackState that = (TrackState) o;
        return Objects.equals(trackStateId, that.trackStateId) &&
                Objects.equals(activity, that.activity) &&
                Objects.equals(state, that.state) &&
                Objects.equals(role, that.role) &&
                Objects.equals(process, that.process);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trackStateId, activity, state, role, process);
    }

    @OneToMany(mappedBy = "trackStateByTrackStateId")
    public List<Track> getTracksByTrackStateId() {
        return tracksByTrackStateId;
    }

    public void setTracksByTrackStateId(List<Track> tracksByTrackStateId) {
        this.tracksByTrackStateId = tracksByTrackStateId;
    }
}
