package co.com.soaint.tracking.api.repository.disposicion.impl;

import co.com.soaint.tracking.api.entities.Track;

import java.util.List;

public interface ITrackFacade {

    List<Track> findTracks(String objectId, String instanceId);

    List<Track> findLastTrack(String objectId);

    Track createTrack(Track trackDTO);

    Track patchTrack(Track trackDTO);

    Track findTrackById(Long id);
}
