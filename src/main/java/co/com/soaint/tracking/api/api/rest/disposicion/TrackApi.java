package co.com.soaint.tracking.api.api.rest.disposicion;

import co.com.soaint.tracking.api.commons.constants.api.disposicion.IEndPointTrack;
import co.com.soaint.tracking.api.commons.domains.generic.TrackDTO;
import co.com.soaint.tracking.api.api.ITrackApi;
import co.com.soaint.tracking.api.service.disposicion.IManagementTrack;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Log4j2
@RestController
@RequestMapping(value = IEndPointTrack.BASE_TRACK)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class TrackApi implements ITrackApi {

    private final IManagementTrack iManagementTrack;

    @Autowired
    public TrackApi(IManagementTrack iManagementTrack) {
        this.iManagementTrack = iManagementTrack;
    }

    @Override
    @PostMapping(IEndPointTrack.CREATE_TRACK)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity<TrackDTO> createTrack(@RequestBody() TrackDTO trackDTO) {
        log.trace("Create track :: creating track");
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrack.createTrack(trackDTO));
    }

    @Override
    @PostMapping(IEndPointTrack.CREATE_TRACK_BATCH)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity createTrackBatch(@RequestBody() List<TrackDTO> trackDTOList) {
        log.trace("Create track :: creating track");
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrack.createTrackBatch(trackDTOList));
    }

    @Override
    @GetMapping(IEndPointTrack.FIND_ALL_TRACKS)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity findTracks(@RequestParam(required = false, value = "objectId") String objectId,
                                     @RequestParam(required = false, value = "instanceId") String instanceId) {
        log.trace("findTracks :: find tracks");
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrack.findTracks(objectId, instanceId));
    }

    @Override
    @PutMapping(IEndPointTrack.PATCH_TRACK)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity<TrackDTO> patchTrackById(@RequestBody() TrackDTO trackDTO, @RequestParam("instanceId") Long instanceId) {
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrack.patchTrackById(trackDTO, instanceId));
    }

    @Override
    @GetMapping(IEndPointTrack.FIND_TRACK_BY_ID)
    public ResponseEntity<TrackDTO> findTrackById(@PathVariable("id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrack.findTrackById(id));
    }

}
