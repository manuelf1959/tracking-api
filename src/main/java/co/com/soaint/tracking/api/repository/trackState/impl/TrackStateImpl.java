package co.com.soaint.tracking.api.repository.trackState.impl;

import co.com.soaint.tracking.api.entities.TrackState;
import co.com.soaint.tracking.api.repository.trackState.TrackStateRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class TrackStateImpl implements ITrackStateFacade {

    private final TrackStateRepository repository;

    @Autowired
    public TrackStateImpl(TrackStateRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<TrackState> findTrackStates() {
        return this.repository.findAll();
    }

    @Override
    public TrackState createTrack(TrackState trackState) {
        return repository.save(trackState);
    }

    @Override
    public TrackState updateTrackById(TrackState trackState, Long id) {
        return null;
    }

    @Override
    public TrackState findTrackStateById(Long id) {
        return this.repository.findById(id);
    }
}
