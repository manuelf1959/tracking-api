package co.com.soaint.tracking.api.service.disposicion.impl;

import co.com.soaint.tracking.api.commons.converter.TrackStateConverter;
import co.com.soaint.tracking.api.commons.domains.generic.TrackStateDTO;
import co.com.soaint.tracking.api.repository.trackState.impl.ITrackStateFacade;
import co.com.soaint.tracking.api.service.disposicion.IManagementTrackState;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class ManagementTrackState implements IManagementTrackState {

    private final Environment environment;
    private final ITrackStateFacade iTrackStateFacade;

    @Autowired
    public ManagementTrackState(Environment environment, ITrackStateFacade iTrackStateFacade) {
        this.environment = environment;
        this.iTrackStateFacade = iTrackStateFacade;
    }

    @Override
    public TrackStateDTO createTrackState(TrackStateDTO trackStateDTO) {
        return TrackStateConverter.convertTrackStateToTrackStateDTO(iTrackStateFacade.createTrack(
                TrackStateConverter.convertTrackStateDtoToTrackState(trackStateDTO)));
    }

    @Override
    public List<TrackStateDTO> findTrackStates() {
        return TrackStateConverter.convertTrackStateDtoListToTrackStateList(iTrackStateFacade.findTrackStates());
    }

    @Override
    public TrackStateDTO updateTrackStateById(TrackStateDTO trackStateDTO, Long id) {
        return null;
    }

    @Override
    public TrackStateDTO findTrackStateById(Long id) {
        return TrackStateConverter.convertTrackStateToTrackStateDTO(iTrackStateFacade.findTrackStateById(id));
    }
}
