package co.com.soaint.tracking.api.repository.disposicion.impl;

import co.com.soaint.tracking.api.commons.exception.business.NoCreatedException;
import co.com.soaint.tracking.api.entities.Track;
import co.com.soaint.tracking.api.repository.disposicion.TrackRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Component
@Log4j2
public class TrackImpl implements ITrackFacade {

    private final TrackRepository repository;

    @Autowired
    public TrackImpl(TrackRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Track> findTracks(String objectId, String instanceId) {
        if (!ObjectUtils.isEmpty(objectId)) {
            return this.repository.findByObjectId(objectId);
        } else if (!ObjectUtils.isEmpty(instanceId)) {
            return this.repository.findByInstanceId(Long.parseLong(instanceId));
        }
        return this.repository.findByObjectId(objectId);
    }

    @Override
    public List<Track> findLastTrack(String objectId) {
        return this.repository.findLastTrack(objectId);
    }

    @Override
    public Track createTrack(Track trackDTO) {
        try {
            return repository.save(trackDTO);
        } catch (Exception ex) {
            throw new NoCreatedException();
        }
    }

    @Override
    public Track patchTrack(Track trackDTO) {
        return repository.save(trackDTO);
    }

    @Override
    public Track findTrackById(Long id) {
        return this.repository.findById(id);
    }
}
