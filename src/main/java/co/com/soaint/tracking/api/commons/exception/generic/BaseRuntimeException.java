package co.com.soaint.tracking.api.commons.exception.generic;

public abstract class BaseRuntimeException extends RuntimeException {

    public BaseRuntimeException() {
    }

    public BaseRuntimeException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
