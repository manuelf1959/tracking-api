package co.com.soaint.tracking.api.commons.converter;

import org.springframework.http.HttpHeaders;

import java.util.HashMap;

public class ClientConverter {

    private ClientConverter() {
    }

    public static HttpHeaders addHeaders(HashMap<String, String> mapHeaders) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(mapHeaders);
        return httpHeaders;
    }
}
