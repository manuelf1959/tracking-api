package co.com.soaint.tracking.api.commons.exception.business;


import co.com.soaint.tracking.api.commons.exception.generic.BaseRuntimeException;

public class UnAuthorizedException extends BaseRuntimeException {

    public UnAuthorizedException() {
    }
}

