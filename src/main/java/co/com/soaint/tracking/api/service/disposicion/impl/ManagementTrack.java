package co.com.soaint.tracking.api.service.disposicion.impl;

import co.com.soaint.tracking.api.adapter.specific.IDependenciesClient;
import co.com.soaint.tracking.api.commons.converter.TrackConverter;
import co.com.soaint.tracking.api.commons.domains.clients.DependencyDTO;
import co.com.soaint.tracking.api.commons.domains.clients.DependencyListDTO;
import co.com.soaint.tracking.api.commons.domains.generic.TrackDTO;
import co.com.soaint.tracking.api.commons.exception.system.SystemException;
import co.com.soaint.tracking.api.entities.Track;
import co.com.soaint.tracking.api.repository.disposicion.impl.ITrackFacade;
import co.com.soaint.tracking.api.service.disposicion.IManagementTrackState;
import co.com.soaint.tracking.api.service.disposicion.IManagementTrack;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.*;


@Component
@Log4j2
public class ManagementTrack implements IManagementTrack {

    private final Environment environment;
    private final ITrackFacade iTrackFacade;
    private final IManagementTrackState iManagementTrackState;
    private final IDependenciesClient iDependenciesClient;

    @Autowired
    public ManagementTrack(Environment environment, ITrackFacade iTrackFacade, IManagementTrackState iManagementTrackState,
                           IDependenciesClient iDependenciesClient) {
        this.environment = environment;
        this.iTrackFacade = iTrackFacade;
        this.iManagementTrackState = iManagementTrackState;
        this.iDependenciesClient = iDependenciesClient;
    }

    @Override
    public List<TrackDTO> findTracks(String objectId, String instanceId) throws SystemException {
        log.trace("ManagementConfigSignature :: consultManualSignature :: manual signature listing");

        List<TrackDTO> trackDTOList = TrackConverter.convertTrackListToTrackDTOList(iTrackFacade.findTracks(objectId, instanceId));
        DependencyListDTO dependencyListDTO = iDependenciesClient.findAllDepencencies();

        trackDTOList.forEach(trackDTO -> {
            Optional<DependencyDTO> dependency = dependencyListDTO.getDependencias()
                    .stream()
                    .filter(dependencyDTO -> dependencyDTO.getCodigo().equalsIgnoreCase(String.valueOf(trackDTO.getSubsection())))
                    .findFirst();
            dependency.ifPresent(dependencyDTO -> trackDTO.setSubsectionObject(dependencyDTO.getNombre()));
        });
        return trackDTOList;
    }

    @Override
    public TrackDTO createTrack(TrackDTO trackDTO) {
        List<Track> lastTrackList = iTrackFacade.findLastTrack(trackDTO.getObjectId());
        if (!lastTrackList.isEmpty()) {
            trackDTO.setStartDate(lastTrackList.get(0).getEndDate());
        }
        trackDTO.setEndDate(new Date());
        trackDTO.setTrackState(iManagementTrackState.findTrackStateById(trackDTO.getTrackStateId()));
        return TrackConverter.convertTrackToTrackDTO(
                iTrackFacade.createTrack(TrackConverter.convertTrackDTOToTrack(trackDTO)));
    }

    @Override
    public List<TrackDTO> createTrackBatch(List<TrackDTO> trackDTOList) {
        List<TrackDTO> trackDTOListResponse = new ArrayList<>();
        trackDTOList.forEach(trackDTO -> {
            trackDTOListResponse.add(createTrack(trackDTO));
        });
        return trackDTOListResponse;
    }

    @Override
    public TrackDTO patchTrackById(TrackDTO trackDTO, Long instanceId) {
        List<TrackDTO> trackDTOList = TrackConverter.convertTrackListToTrackDTOList(
                iTrackFacade.findTracks(null, instanceId.toString()));
        trackDTOList.forEach(track -> {
            track.setObjectId(trackDTO.getObjectId());
            iTrackFacade.patchTrack(TrackConverter.convertTrackDTOToTrack(track));
        });
        return null;
    }

    @Override
    public TrackDTO findTrackById(Long id) {
        return TrackConverter.convertTrackToTrackDTO(iTrackFacade.findTrackById(id));
    }

}



