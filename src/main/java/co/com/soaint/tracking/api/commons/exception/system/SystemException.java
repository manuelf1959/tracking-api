package co.com.soaint.tracking.api.commons.exception.system;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SystemException extends RuntimeException {

    private Exception ex;

    public SystemException() {
    }
}

