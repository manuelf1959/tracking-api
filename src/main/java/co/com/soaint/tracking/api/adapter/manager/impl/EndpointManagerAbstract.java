package co.com.soaint.tracking.api.adapter.manager.impl;

import co.com.soaint.tracking.api.commons.converter.ClientConverter;
import co.com.soaint.tracking.api.adapter.manager.IEndpointManager;
import co.com.soaint.tracking.api.configuration.EndpointConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Component
public abstract class EndpointManagerAbstract implements IEndpointManager {

    private EndpointConfig endpointConfig;

    @Autowired
    public EndpointManagerAbstract(EndpointConfig endpointConfig) {
        this.endpointConfig = endpointConfig;
    }


    public ResponseEntity endpointConsumerClient(final String pathEndpoint,
                                                 final Class<?> typeResponse,
                                                 final HttpMethod method,
                                                 final Object body) {

        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders httpHeadersConsumer = endpointConfig.createAuthenticationHeaders();
        return clientConsumer.exchange(pathEndpoint, method, new HttpEntity<>(body, httpHeadersConsumer), typeResponse);
    }

    public ResponseEntity endpointConsumerClient(final String pathEndpoint,
                                                 final Class<?> typeResponse,
                                                 final HttpMethod method,
                                                 final HashMap<String, String> headers) {

        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders httpHeadersConsumer = ClientConverter.addHeaders(headers);
        return clientConsumer.exchange(pathEndpoint, method, new HttpEntity<>(httpHeadersConsumer), typeResponse);
    }

    public ResponseEntity endpointConsumerClient(final String pathEndpoint,
                                                 final Class<?> typeResponse,
                                                 final HttpMethod method) {

        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders httpHeadersConsumer = endpointConfig.createAuthenticationHeaders();
        return clientConsumer.exchange(pathEndpoint, method, new HttpEntity<>(httpHeadersConsumer), typeResponse);
    }

    public ResponseEntity endpointConsumerClient(final String pathEndpoint,
                                                 final Class<?> typeResponse,
                                                 final HttpMethod method,
                                                 final Object body,
                                                 final HashMap<String, String> headers) {
        try {
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            RestTemplate clientConsumer = new RestTemplate(requestFactory);
            HttpHeaders httpHeadersConsumer = ClientConverter.addHeaders(headers);
            return clientConsumer.exchange(pathEndpoint, method, new HttpEntity<>(body, httpHeadersConsumer), typeResponse);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error llamando al api " + e);
        }
    }

}
