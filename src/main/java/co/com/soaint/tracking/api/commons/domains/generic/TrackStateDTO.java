package co.com.soaint.tracking.api.commons.domains.generic;

import lombok.*;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class TrackStateDTO implements Serializable  {
    private Long trackStateId;
    private String activity;
    private String state;
    private String role;
    private String process;
}
