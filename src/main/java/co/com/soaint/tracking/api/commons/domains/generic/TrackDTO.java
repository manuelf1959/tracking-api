package co.com.soaint.tracking.api.commons.domains.generic;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class TrackDTO implements Serializable {
    private Long trackId;
    private String objectId;
    private String objectType;
    private String user;
    private Date startDate;
    private Date endDate;
    private TrackStateDTO trackState;
    private long trackStateId;
    private long subsection;
    private String subsectionObject;
    private long instanceId;
}
