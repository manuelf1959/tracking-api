package co.com.soaint.tracking.api.api;

import co.com.soaint.tracking.api.commons.domains.generic.TrackDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ITrackApi {

    public ResponseEntity<TrackDTO> createTrack(TrackDTO trackDTO);

    public ResponseEntity createTrackBatch(List<TrackDTO> trackDTOList);

    public ResponseEntity<TrackDTO> findTracks(String objectId, String instanceId);

    public ResponseEntity<TrackDTO> patchTrackById(TrackDTO trackDTO, Long id);

    public ResponseEntity<TrackDTO> findTrackById(Long id);
}
