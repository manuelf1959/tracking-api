package co.com.soaint.tracking.api.commons.domains.clients;

import lombok.*;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class DependencyDTO implements Serializable {

    private long id;
    private String codigo;
    private String nombre;
    private long ideSede;
    private String codSede;
    private String nomSede;
    private String estado;
    private boolean radicadora;

}
