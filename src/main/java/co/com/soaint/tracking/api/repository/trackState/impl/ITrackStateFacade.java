package co.com.soaint.tracking.api.repository.trackState.impl;

import co.com.soaint.tracking.api.entities.TrackState;

import java.util.List;

public interface ITrackStateFacade {

    List<TrackState> findTrackStates();

    TrackState createTrack(TrackState trackState);

    TrackState updateTrackById(TrackState trackState, Long id);

    TrackState findTrackStateById(Long id);
}
