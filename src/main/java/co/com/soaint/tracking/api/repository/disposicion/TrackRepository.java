package co.com.soaint.tracking.api.repository.disposicion;

import co.com.soaint.tracking.api.entities.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrackRepository extends JpaRepository<Track, String> {

    @Query("select n from Track n where n.objectId like %:objectId% order by n.endDate desc")
    List<Track> findByObjectId(@Param("objectId") final String objectId);

    @Query("select n from Track n where n.instanceId = :instanceId order by n.endDate desc")
    List<Track> findByInstanceId(@Param("instanceId") final Long instanceId);

    @Query("select n from Track n where n.objectId = :objectId order by n.endDate desc")
    List<Track> findLastTrack(@Param("objectId") final String objectId);

    @Query("select n from Track n where n.trackId = :id")
    Track findById(@Param("id") final Long id);

}
