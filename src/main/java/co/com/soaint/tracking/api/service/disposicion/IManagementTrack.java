package co.com.soaint.tracking.api.service.disposicion;


import co.com.soaint.tracking.api.commons.domains.generic.TrackDTO;

import java.util.List;

public interface IManagementTrack {

    TrackDTO createTrack(TrackDTO trackDTO);

    List<TrackDTO> createTrackBatch(List<TrackDTO> trackDTOList);

    List<TrackDTO> findTracks(String objectId,String instanceId);

    TrackDTO patchTrackById(TrackDTO trackDTO, Long id);

    TrackDTO findTrackById(Long id);

}
