package co.com.soaint.tracking.api.api;

import co.com.soaint.tracking.api.commons.domains.generic.TrackStateDTO;
import org.springframework.http.ResponseEntity;

public interface ITrackStateApi {

    public ResponseEntity<TrackStateDTO> createTrackState(TrackStateDTO TrackStateDTO);

    public ResponseEntity<TrackStateDTO> findTrackStates();

    public ResponseEntity<TrackStateDTO> updateTrackStateById(TrackStateDTO TrackStateDTO, Long id);

    public ResponseEntity<TrackStateDTO> findTrackStateById(Long id);

}
