package co.com.soaint.tracking.api.service.disposicion;

import co.com.soaint.tracking.api.commons.domains.generic.TrackStateDTO;

import java.util.List;

public interface IManagementTrackState {

    TrackStateDTO createTrackState(TrackStateDTO trackStateDTO);

    List<TrackStateDTO> findTrackStates();

    TrackStateDTO updateTrackStateById(TrackStateDTO trackStateDTO, Long id);

    TrackStateDTO findTrackStateById(Long id);
}
