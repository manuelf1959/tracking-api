package co.com.soaint.tracking.api.commons.constants.api.disposicion;

public interface IEndPointTrack {
    String BASE_TRACK = "soaint-soadoc-core-enterprise/tracking-api/v1";

    String FIND_ALL_TRACKS = "/tracks";
    String CREATE_TRACK = "/tracks";
    String CREATE_TRACK_BATCH = "/tracks/batch";
    String PATCH_TRACK = "/tracks";
    String FIND_TRACK_BY_ID = "/tracks/{id}";
}
