package co.com.soaint.tracking.api.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sdc_track")
public class Track {
    private Long trackId;
    private Long subsection;
    private Date startDate;
    private Date endDate;
    private String user;
    private String objectType;
    private String objectId;
    private Long instanceId;
    private TrackState trackStateByTrackStateId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "track_id")
    public Long getTrackId() {
        return trackId;
    }

    public void setTrackId(Long trackId) {
        this.trackId = trackId;
    }

    @Basic
    @Column(name = "subsection")
    public Long getSubsection() {
        return subsection;
    }

    public void setSubsection(Long subsection) {
        this.subsection = subsection;
    }

    @Basic
    @Column(name = "start_date")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "end_date")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "track_user")
    public String getUser() {
        return user;
    }

    public void setUser(String trackUser) {
        this.user = trackUser;
    }

    @Basic
    @Column(name = "object_type")
    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    @Basic
    @Column(name = "object_id")
    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "instance_id")
    public Long getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Long instanceId) {
        this.instanceId = instanceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Track track = (Track) o;
        return Objects.equals(trackId, track.trackId) &&
                Objects.equals(subsection, track.subsection) &&
                Objects.equals(startDate, track.startDate) &&
                Objects.equals(endDate, track.endDate) &&
                Objects.equals(user, track.user) &&
                Objects.equals(objectType, track.objectType) &&
                Objects.equals(objectId, track.objectId) &&
                Objects.equals(instanceId, track.instanceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trackId, subsection, startDate, endDate, user, objectType, objectId, instanceId);
    }

    @ManyToOne
    @JoinColumn(name = "track_state_id", referencedColumnName = "track_state_id", nullable = false)
    public TrackState getTrackStateByTrackStateId() {
        return trackStateByTrackStateId;
    }

    public void setTrackStateByTrackStateId(TrackState trackStateByTrackStateId) {
        this.trackStateByTrackStateId = trackStateByTrackStateId;
    }
}
