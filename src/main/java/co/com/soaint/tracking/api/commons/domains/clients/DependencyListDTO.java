package co.com.soaint.tracking.api.commons.domains.clients;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class DependencyListDTO implements Serializable {
    private List<DependencyDTO> dependencias;
}
