package co.com.soaint.tracking.api.api.rest.disposicion;

import co.com.soaint.tracking.api.commons.constants.api.disposicion.IEndPointTrackState;
import co.com.soaint.tracking.api.commons.domains.generic.TrackStateDTO;
import co.com.soaint.tracking.api.api.ITrackStateApi;
import co.com.soaint.tracking.api.service.disposicion.IManagementTrackState;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Log4j2
@RestController
@RequestMapping(value = IEndPointTrackState.BASE_TRACK_STATE)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class TrackStateApi implements ITrackStateApi {


    private final IManagementTrackState iManagementTrackState;

    @Autowired
    public TrackStateApi(IManagementTrackState iManagementTrackState) {
        this.iManagementTrackState = iManagementTrackState;
    }

    @Override
    @PostMapping(IEndPointTrackState.CREATE_TRACK_STATE)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity<TrackStateDTO> createTrackState(@RequestBody() TrackStateDTO TrackStateDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrackState.createTrackState(TrackStateDTO));
    }

    @Override
    @GetMapping(IEndPointTrackState.FIND_ALL_TRACK_STATES)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity findTrackStates() {
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrackState.findTrackStates());
    }

    @Override
    @PatchMapping(IEndPointTrackState.UPDATE_TRACK_STATE)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity<TrackStateDTO> updateTrackStateById(@RequestBody() TrackStateDTO TrackStateDTO,
                                                              @PathVariable("id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrackState.updateTrackStateById(TrackStateDTO, id));
    }

    @Override
    @GetMapping(IEndPointTrackState.FIND_TRACK_STATE_BY_ID)
    public ResponseEntity<TrackStateDTO> findTrackStateById(@PathVariable("id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(iManagementTrackState.findTrackStateById(id));
    }
}
