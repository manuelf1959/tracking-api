package co.com.soaint.tracking.api.commons.converter;

import co.com.soaint.tracking.api.commons.domains.generic.TrackDTO;
import co.com.soaint.tracking.api.entities.Track;

import java.util.ArrayList;
import java.util.List;

public class TrackConverter {

    public static List<TrackDTO> convertTrackListToTrackDTOList(List<Track> trackList) {
        List<TrackDTO> trackListDTO = new ArrayList<>();
        trackList.forEach(trackDTO -> {
            trackListDTO.add(
                    TrackDTO.builder()
                            .endDate(trackDTO.getEndDate())
                            .startDate(trackDTO.getStartDate())
                            .instanceId(trackDTO.getInstanceId())
                            .objectId(trackDTO.getObjectId())
                            .objectType(trackDTO.getObjectType())
                            .subsection(trackDTO.getSubsection())
                            .trackId(trackDTO.getTrackId())
                            .trackState(TrackStateConverter.convertTrackStateToTrackStateDTO(trackDTO.getTrackStateByTrackStateId()))
                            .user(trackDTO.getUser())
                            .build()
            );
        });
        return trackListDTO;
    }

    public static List<Track> convertTrackDTOListToTrack(List<TrackDTO> trackDTOList) {
        List<Track> trackList = new ArrayList<>();
        trackDTOList.forEach(trackDTO -> {
            trackList.add(
                    Track.builder()
                            .instanceId(trackDTO.getInstanceId())
                            .objectId(trackDTO.getObjectId())
                            .objectType(trackDTO.getObjectType())
                            .subsection(trackDTO.getSubsection())
                            .trackId(trackDTO.getTrackId())
                            .user(trackDTO.getUser())
                            .build()
            );
        });
        return trackList;
    }

    public static Track convertTrackDTOToTrack(TrackDTO trackDTO) {
        return Track.builder()
                .endDate(trackDTO.getEndDate())
                .startDate(trackDTO.getStartDate())
                .instanceId(trackDTO.getInstanceId())
                .objectId(trackDTO.getObjectId())
                .objectType(trackDTO.getObjectType())
                .subsection(trackDTO.getSubsection())
                .trackId(trackDTO.getTrackId())
                .trackStateByTrackStateId(TrackStateConverter.convertTrackStateDtoToTrackState(trackDTO.getTrackState()))
                .user(trackDTO.getUser())
                .build();
    }

    public static TrackDTO convertTrackToTrackDTO(Track track) {
        return TrackDTO.builder()
                .endDate(track.getEndDate())
                .startDate(track.getStartDate())
                .instanceId(track.getInstanceId())
                .objectId(track.getObjectId())
                .objectType(track.getObjectType())
                .subsection(track.getSubsection())
                .trackId(track.getTrackId())
                .trackState(TrackStateConverter.convertTrackStateToTrackStateDTO(track.getTrackStateByTrackStateId()))
                .user(track.getUser())
                .build();
    }

}
