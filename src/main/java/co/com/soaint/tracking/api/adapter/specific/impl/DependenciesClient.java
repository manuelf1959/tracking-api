package co.com.soaint.tracking.api.adapter.specific.impl;


import co.com.soaint.tracking.api.commons.domains.clients.DependencyListDTO;
import co.com.soaint.tracking.api.commons.exception.business.NotFoundException;
import co.com.soaint.tracking.api.adapter.manager.impl.EndpointManagerAbstract;
import co.com.soaint.tracking.api.adapter.specific.IDependenciesClient;
import co.com.soaint.tracking.api.configuration.EndpointConfig;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

@Component
@Log4j2
public class DependenciesClient extends EndpointManagerAbstract implements IDependenciesClient {


    private final Environment environment;

    public DependenciesClient(EndpointConfig endpointConfig, Environment environment) {
        super(endpointConfig);
        this.environment = environment;
    }

    @Override
    public DependencyListDTO findAllDepencencies() {
        try {
            log.info("SignatureClient :: findAllDocument :: find all document by folder id");
            String urlFolder = environment.getProperty("endpoint.correspondencia.business.services") + "/dependencia-web-api/dependencias";
            ResponseEntity<String> response = endpointConsumerClient(urlFolder, String.class, HttpMethod.GET);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                Gson gson = new Gson();
                Type type = new TypeToken<DependencyListDTO>() {
                }.getType();
                return gson.fromJson(response.getBody(), type);
            }
            log.error("SignatureClient :: findAllDocument :: not found documents");
            throw new NotFoundException();
        } catch (Exception e) {
            log.error("SignatureClient :: findAllDocument :: Error " + e);
            throw new NotFoundException();
        }
    }


}

