package co.com.soaint.tracking.api.commons.converter;

import co.com.soaint.tracking.api.commons.domains.generic.TrackStateDTO;
import co.com.soaint.tracking.api.entities.TrackState;

import java.util.ArrayList;
import java.util.List;

public class TrackStateConverter {

    public static List<TrackStateDTO> convertTrackStateDtoListToTrackStateList(List<TrackState> trackStateList) {
        List<TrackStateDTO> trackStateListDTO = new ArrayList<>();
        trackStateList.forEach(trackState -> {
            trackStateListDTO.add(
                    TrackStateDTO.builder()
                            .activity(trackState.getActivity())
                            .process(trackState.getProcess())
                            .role(trackState.getRole())
                            .state(trackState.getState())
                            .trackStateId(trackState.getTrackStateId())
                            .build()
            );
        });
        return trackStateListDTO;
    }

    public static TrackState convertTrackStateDtoToTrackState(TrackStateDTO trackStateDTO) {
        return TrackState.builder()
                .activity(trackStateDTO.getActivity())
                .process(trackStateDTO.getProcess())
                .role(trackStateDTO.getRole())
                .state(trackStateDTO.getState())
                .trackStateId(trackStateDTO.getTrackStateId())
                .build();
    }

    public static TrackStateDTO convertTrackStateToTrackStateDTO(TrackState trackState) {
        return TrackStateDTO.builder()
                .activity(trackState.getActivity())
                .process(trackState.getProcess())
                .role(trackState.getRole())
                .state(trackState.getState())
                .trackStateId(trackState.getTrackStateId())
                .build();
    }
}
