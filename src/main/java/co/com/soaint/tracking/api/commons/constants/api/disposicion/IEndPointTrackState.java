package co.com.soaint.tracking.api.commons.constants.api.disposicion;

public interface IEndPointTrackState {
    String BASE_TRACK_STATE = "soaint-soadoc-core-enterprise/tracking-api/v1";

    String FIND_ALL_TRACK_STATES = "/trackStates";
    String CREATE_TRACK_STATE = "/trackStates";
    String UPDATE_TRACK_STATE = "/trackStates/{id}";
    String FIND_TRACK_STATE_BY_ID = "/trackStates/{id}";
}
