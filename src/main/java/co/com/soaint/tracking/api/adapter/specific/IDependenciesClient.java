package co.com.soaint.tracking.api.adapter.specific;

import co.com.soaint.tracking.api.commons.domains.clients.DependencyListDTO;

public interface IDependenciesClient {

    DependencyListDTO findAllDepencencies();

}
